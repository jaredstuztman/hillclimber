-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then
    require("lldebugger").start()
end

local composer = require("composer")
display.setDefault("textureWrapX", "repeat")
display.setDefault("textureWrapY", "repeat")
local createGround = require("ground")
local vehicle = require("vehicle")
local getGround = createGround.getCountrySide()
local physics = require("physics")
physics.start()
-- physics.setGravity(0, 3)
-- physics.setDrawMode("debug")

local backGroundG = display.newGroup()
local roadGroup = display.newGroup()
local truck = display.newGroup()
local groundGroup = display.newGroup()

local backGround = display.newImageRect("sky.png", display.actualContentWidth, display.actualContentHeight)
backGround.x, backGround.y = display.contentCenterX, display.contentCenterY
backGroundG:insert(backGround)

local backMountainZ = 0.95
local backMountains = display.newRect(display.contentCenterX, display.contentHeight * 0.8, display.contentWidth * 3, display.contentHeight)
backMountains.fill = {type = "image", filename = "back_mountain.png"}
local MscaleFactorX = 256 / backMountains.width * 2
local MscaleFactorY = 256 / 255
backMountains.fill.scaleX = MscaleFactorX
backMountains.fill.scaleY = MscaleFactorY
backGroundG:insert(backMountains)

local groundShape = {}
local hightestPoint = getGround[2]
local destoyVal, createVal = 1200, -200
local truckStartPositionX, truckStartPositionY = display.contentWidth * 0.7, display.contentHeight * 0.8

for i = 1, 12 do
    groundShape[i] = getGround[i]
    if (i % 2 == 0 and hightestPoint > groundShape[i]) then
        hightestPoint = groundShape[i]
    end
end
groundShape[#groundShape + 1] = groundShape[#groundShape - 1]
groundShape[#groundShape + 1] = hightestPoint + 1000
groundShape[#groundShape + 1] = groundShape[1]
groundShape[#groundShape + 1] = hightestPoint + 1000

local ground = display.newPolygon(1200 - (groundShape[1] - groundShape[#groundShape - 5]) / 2, truckStartPositionY + 500 + 50, groundShape)
groundGroup:insert(ground)
ground.fill = {type = "image", filename = "dirt.png"}
local scaleFactorX = 128 / ground.width
local scaleFactorY = 128 / ground.height
ground.fill.scaleX = scaleFactorX
ground.fill.scaleY = scaleFactorY

local road = {}
local roadZ = 0.3
local groundCloneZ = roadZ / 2
local roadWidth, roadHeight
local roadX, roadY
local RscaleFactorX
local roadFillShiftX = 0
local topGroundNum
local roadShiftedX = ground.x + ground.width / 2 - groundShape[1]
local roadShiftedY = ground.y - ground.height / 2 - hightestPoint
local cameraFocusX, cameraFocusY = display.contentCenterX, display.contentCenterY

for i = 1, #groundShape / 2 - 3 do
    if (i > 1) then
        roadFillShiftX = (roadFillShiftX + ((groundShape[i * 2 - 3] - groundShape[i * 2 + 1]) / 2) / 128) % 1
    end
    roadWidth = groundShape[i * 2 - 1] - groundShape[i * 2 + 1]
    roadHeight = 0
    roadX = groundShape[i * 2 - 1] - roadWidth / 2 + roadShiftedX
    roadY = groundShape[i * 2] - roadHeight / 2 + roadShiftedY
    road[i] = display.newRect(roadX, roadY, roadWidth, roadHeight)
    road[i].fill = {type = "image", filename = "dirt_road.png"}
    RscaleFactorX = 128 / road[i].width
    road[i].fill.scaleX = RscaleFactorX
    road[i].fill.x = -roadFillShiftX
    road[#road].path.x1, road[#road].path.x2, road[#road].path.x3, road[#road].path.x4, road[#road].path.y1, road[#road].path.y2, road[#road].path.y3, road[#road].path.y4 =
        0,
        0,
        0,
        0,
        0,
        groundShape[i * 2 + 2] - groundShape[i * 2],
        0,
        0
    if (getGround[i * 2 + 1] + roadShiftedX > display.contentCenterX) then
        roadGroup:insert(road[i])
        topGroundNum = i
    else
        roadGroup:insert(road[i], #road)
    end
end

local reverseLimit = display.newRect(destoyVal - 400, display.contentCenterY, 10, 1000)
reverseLimit.alpha = 0
physics.addBody(reverseLimit, "static")

local groundShapeClone = {}
for i = 1, #groundShape do
    groundShapeClone[i] = groundShape[i] * (1 - groundCloneZ)
end
local groundClone = display.newPolygon(cameraFocusX + (ground.x - cameraFocusX) * (1 - groundCloneZ), cameraFocusY + (ground.y - cameraFocusY) * (1 - groundCloneZ), groundShapeClone)
groundClone.alpha = 0
physics.addBody(groundClone, "static", {friction = 0.1})

truck.scale = (1 - groundCloneZ)
physics.setGravity(0, 10 * truck.scale)
local thisTruck = vehicle.truck(truck, truckStartPositionX, truckStartPositionY)

local time = 0
local forward = 0
local secondery = 0
local truckMX = 0
local truckMY = 0
local cameraFrequencyX = 0.1
local cameraFrequencyY = 0.01
local groundPX = ground.x
local groundPY = ground.y
local previousHP = hightestPoint
local previousGS1 = groundShape[1]
local startVal = 1
local previousFillX, previousFillY = ground.fill.x, ground.fill.y

local move = function()
    truck.body:applyTorque(0.01 * forward * truck.scale ^ 3)
    truck.backTire:applyTorque(0.01 * -forward * truck.scale ^ 3)
    truckMX = (truckStartPositionX - truck.body.x) * math.abs(truckStartPositionX - truck.body.x) / 200
    truckMY = (truckStartPositionY - truck.body.y) * math.abs(truckStartPositionY - truck.body.y) / 8
    truck.body.x = truck.body.x + truckMX * cameraFrequencyX * (1 - groundCloneZ)
    truck.body.y = truck.body.y + truckMY * cameraFrequencyY * (1 - groundCloneZ)
    truck.backTire.x = truck.backTire.x + truckMX * cameraFrequencyX * (1 - groundCloneZ)
    truck.backTire.y = truck.backTire.y + truckMY * cameraFrequencyY * (1 - groundCloneZ)
    truck.frontTire.x = truck.frontTire.x + truckMX * cameraFrequencyX * (1 - groundCloneZ)
    truck.frontTire.y = truck.frontTire.y + truckMY * cameraFrequencyY * (1 - groundCloneZ)
    ground.x = ground.x + truckMX * cameraFrequencyX
    ground.y = ground.y + truckMY * cameraFrequencyY
    backMountains.x = backMountains.x + truckMX * cameraFrequencyX * (1 - backMountainZ)
    backMountains.y = backMountains.y + truckMY * cameraFrequencyY * (1 - backMountainZ)
    if (backMountains.x - backMountains.width / 2 > display.contentCenterX - display.actualContentWidth / 2) then
        backMountains.x = backMountains.x - 512
    end
    if (reverseLimit.x <= destoyVal - 200 or truckMX < 0) then
        reverseLimit.x = reverseLimit.x + truckMX * cameraFrequencyX
    end
    groundClone.x = cameraFocusX + (ground.x - cameraFocusX) * (1 - groundCloneZ)
    groundClone.y = cameraFocusY + (ground.y - cameraFocusY) * (1 - groundCloneZ)
    for i = 1, #road do
        roadShiftedX = ground.x + ground.width / 2 - groundShape[1]
        roadShiftedY = ground.y - ground.height / 2 - hightestPoint
        roadWidth = groundShape[i * 2 - 1] - groundShape[i * 2 + 1]
        roadX = groundShape[i * 2 - 1] - roadWidth / 2 + roadShiftedX
        roadY = groundShape[i * 2] - roadHeight / 2 + roadShiftedY
        road[i].x, road[i].y = roadX, roadY
        road[i].path.x1 = (cameraFocusX - (groundShape[i * 2 + 1] + roadShiftedX)) * roadZ
        road[i].path.x4 = (cameraFocusX - (groundShape[i * 2 - 1] + roadShiftedX)) * roadZ
        road[i].path.y1 = (cameraFocusY - (groundShape[i * 2 + 2] + roadShiftedY)) * roadZ - (groundShape[i * 2] - groundShape[i * 2 + 2])
        road[i].path.y4 = (cameraFocusY - (groundShape[i * 2] + roadShiftedY)) * roadZ
    end
    if (groundShape[topGroundNum * 2 + 1] ~= nil) then
        if (groundShape[topGroundNum * 2 + 1] + roadShiftedX > display.contentCenterX) then
            topGroundNum = topGroundNum + 1
        elseif (groundShape[topGroundNum * 2 - 1] + roadShiftedX < display.contentCenterX) then
            topGroundNum = topGroundNum - 1
        end
        if (road[topGroundNum]) then
            roadGroup:insert(road[topGroundNum])
        end
    end
end

local updatePolygon = function()
    if (ground.x - ground.width / 2 > createVal and getGround[#groundShape - 3 + startVal - 1]) then --create
        previousHP = hightestPoint
        table.insert(groundShape, #groundShape - 3, getGround[#groundShape - 3 + startVal - 1])
        table.insert(groundShape, #groundShape - 3, getGround[#groundShape - 3 + startVal - 1])
        if (groundShape[#groundShape - 4] < hightestPoint) then
            hightestPoint = groundShape[#groundShape - 4]
        end
        groundShape[#groundShape - 3] = groundShape[#groundShape - 5]
        groundShape[#groundShape - 2] = hightestPoint + 1000
        groundShape[#groundShape] = hightestPoint + 1000
        groundPX = ground.x
        groundPY = ground.y
        display.remove(ground)
        ground = display.newPolygon(groundPX - (groundShape[#groundShape - 7] - groundShape[#groundShape - 5]) / 2, groundPY + (hightestPoint - previousHP), groundShape)
        ground.fill = {type = "image", filename = "dirt.png"}
        scaleFactorX = 128 / ground.width
        scaleFactorY = 128 / ground.height
        ground.fill.scaleX = scaleFactorX
        ground.fill.scaleY = scaleFactorY
        ground.fill.x = (previousFillX - (groundShape[#groundShape - 7] - groundShape[#groundShape - 5]) / 2 / 128) % 1
        ground.fill.y = (previousFillY + (hightestPoint - previousHP) / 128) % 1
        previousFillX, previousFillY = ground.fill.x, ground.fill.y
        groundGroup:insert(ground)
        roadShiftedX = ground.x + ground.width / 2 - groundShape[1]
        roadShiftedY = ground.y - ground.height / 2 - hightestPoint
        roadFillShiftX = (roadFillShiftX + ((groundShape[(#road + 1) * 2 - 3] - groundShape[(#road + 1) * 2 + 1]) / 2) / 128) % 1
        roadWidth = groundShape[#road * 2 + 1] - groundShape[#road * 2 + 3]
        roadHeight = 0
        roadX = groundShape[#road * 2 + 1] - roadWidth / 2 + roadShiftedX
        roadY = groundShape[#road * 2 + 2] - roadHeight / 2 + roadShiftedY
        road[#road + 1] = display.newRect(roadX, roadY, roadWidth, roadHeight)
        road[#road].fill = {type = "image", filename = "dirt_road.png"}
        RscaleFactorX = 128 / road[#road].width
        road[#road].fill.scaleX = RscaleFactorX
        road[#road].fill.x = -roadFillShiftX
        road[#road].path.y2 = groundShape[#road * 2 + 2] - groundShape[#road * 2]
        road[#road].path.x1, road[#road].path.x2, road[#road].path.x3, road[#road].path.x4, road[#road].path.y1, road[#road].path.y3, road[#road].path.y4 = 0, 0, 0, 0, 0, 0, 0
        if (groundShape[#road * 2 + 1] + roadShiftedX > display.contentCenterX) then
            roadGroup:insert(road[#road])
            topGroundNum = #road
        else
            roadGroup:insert(road[#road])
            road[#road]:toBack()
        end
        display.remove(groundClone)
        for i = 1, #groundShape do
            groundShapeClone[i] = groundShape[i] * (1 - groundCloneZ)
        end
        table.remove(groundShapeClone, 1)
        table.remove(groundShapeClone, 1)
        groundClone = display.newPolygon(cameraFocusX + (ground.x - cameraFocusX) * (1 - groundCloneZ), cameraFocusY + (ground.y - cameraFocusY) * (1 - groundCloneZ), groundShapeClone)
        groundClone.alpha = 0
        physics.addBody(groundClone, "static", {friction = 0.1})
    end
    if (ground.x + ground.width / 2 > destoyVal and #groundShape > 7) then --destroy
        startVal = startVal + 2
        previousHP = hightestPoint
        if (hightestPoint == groundShape[2]) then
            hightestPoint = groundShape[4]
            for i = 2, #groundShape / 2 do
                if (groundShape[i * 2] < hightestPoint) then
                    hightestPoint = groundShape[i * 2]
                end
            end
        end
        previousGS1 = groundShape[1]
        table.remove(groundShape, 1)
        table.remove(groundShape, 1)
        groundShape[#groundShape - 1] = groundShape[1]
        groundShape[#groundShape] = hightestPoint + 1000
        groundShape[#groundShape - 2] = hightestPoint + 1000
        groundPX = ground.x
        groundPY = ground.y
        display.remove(ground)
        ground = display.newPolygon(groundPX - (previousGS1 - groundShape[1]) / 2, groundPY + (hightestPoint - previousHP), groundShape)
        ground.fill = {type = "image", filename = "dirt.png"}
        scaleFactorX = 128 / ground.width
        scaleFactorY = 128 / ground.height
        ground.fill.scaleX = scaleFactorX
        ground.fill.scaleY = scaleFactorY
        ground.fill.x = (previousFillX - (previousGS1 - groundShape[1]) / 2 / 128) % 1
        ground.fill.y = (previousFillY + (hightestPoint - previousHP) / 128) % 1
        previousFillX, previousFillY = ground.fill.x, ground.fill.y
        groundGroup:insert(ground)
        display.remove(road[1])
        table.remove(road, 1)
        display.remove(groundClone)
        for i = 1, #groundShape do
            groundShapeClone[i] = groundShape[i] * (1 - groundCloneZ)
        end
        table.remove(groundShapeClone, 1)
        table.remove(groundShapeClone, 1)
        groundClone = display.newPolygon(cameraFocusX + (ground.x - cameraFocusX) * (1 - groundCloneZ), cameraFocusY + (ground.y - cameraFocusY) * (1 - groundCloneZ), groundShapeClone)
        groundClone.alpha = 0
        physics.addBody(groundClone, "static", {friction = 0.1})
    end
end

local update = function()
    time = time + 1
    move()
    updatePolygon()
end

local keyEvent = function(event)
    if (event.keyName == "left") then
        if (event.phase == "down") then
            if (forward == -1) then
                secondery = -1
            end
            forward = 1
        else
            if (forward == 1) then
                forward = secondery
            end
            secondery = 0
        end
    elseif (event.keyName == "right") then
        if (event.phase == "down") then
            if (forward == 1) then
                secondery = 1
            end
            forward = -1
        else
            if (forward == -1) then
                forward = secondery
            end
            secondery = 0
        end
    end
    if (event.keyName == "up") then
        truck.body.y = truck.body.y - 100
        truck.backTire.y = truck.backTire.y - 100
        truck.frontTire.y = truck.frontTire.y - 100
    end
end
Runtime:addEventListener("key", keyEvent)
Runtime:addEventListener("enterFrame", update)
