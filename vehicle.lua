-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
local rtn = {}

rtn.truck = function(truck, x, y)
    truck.body = display.newImageRect("truck_body.png", 46.2 * truck.scale, 17.4 * truck.scale)
    truck.body.shapeBottom = {23 * truck.scale, -2 * truck.scale, 23 * truck.scale, 8 * truck.scale, -20 * truck.scale, 8 * truck.scale, -23 * truck.scale, 6 * truck.scale, -23 * truck.scale, -2 * truck.scale}
    truck.body.shapeTop = {10 * truck.scale, -8 * truck.scale, 10 * truck.scale, -2 * truck.scale, -12 * truck.scale, -2 * truck.scale, -4 * truck.scale, -8 * truck.scale}
    truck.body.x, truck.body.y = x, y
    truck.backTire = display.newImageRect("truck_tire.png", 8 * truck.scale, 8 * truck.scale)
    truck.backTire.x, truck.backTire.y = truck.body.x + 15 * truck.scale, truck.body.y + 10 * truck.scale
    truck.frontTire = display.newImageRect("truck_tire.png", 8 * truck.scale, 8 * truck.scale)
    truck.frontTire.x, truck.frontTire.y = truck.body.x - 13 * truck.scale, truck.body.y + 10 * truck.scale
    truck:insert(truck.body)
    truck:insert(truck.backTire)
    truck:insert(truck.frontTire)
    physics.addBody(truck.body, {shape = truck.body.shapeBottom, friction = 0.1}, {shape = truck.body.shapeTop, friction = 0.1})
    physics.addBody(truck.backTire, {radius = 4 * truck.scale, friction = 10})
    physics.addBody(truck.frontTire, {radius = 4 * truck.scale, friction = 10})
    truck.backAxle = physics.newJoint("wheel", truck.body, truck.backTire, truck.backTire.x, truck.backTire.y, 0, 20)
    truck.frontAxle = physics.newJoint("wheel", truck.body, truck.frontTire, truck.frontTire.x, truck.backTire.y, 0, 20)
    truck.backAxle.springFrequency = 10
    truck.backAxle.springDampingRatio = 0
    truck.frontAxle.springFrequency = 10
    truck.frontAxle.springDampingRatio = 0
end

return rtn
