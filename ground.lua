-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
local json = require("json")
local path = "/Users/jared/Library/Application Support/Corona Simulator/test-EBD92ABE5BA4B5978A81052F0A631FB8/Documents/roads.json"
local file, errorString = io.open(path, "r")
local jsonRoadsContents

if not file then
    print("File error: " .. errorString)
else
    jsonRoadsContents = file:read("*a")
    jsonRoadsContents = json.decode(jsonRoadsContents)
    for i = 1, #jsonRoadsContents do
        if not jsonRoadsContents[i] then
            for I = 0, #jsonRoadsContents - i - 1 do
                table.remove(jsonRoadsContents, i + I)
            end
            break
        end
    end
    io.close(file)
end

file = nil

local rtn = {}

local getGround = function(...)
    --default (stage,start array index,end array index)
    local d_ground = jsonRoadsContents
    local startP = 1
    local startX
    local endP
    local endX
    local rtnGround = {}
    if (type(arg[1]) == "string") then
        for i = 1, #arg, 2 do
            if (arg[i] == "stage") then
                d_ground = arg[i + 1]
            end
        end
        startX = d_ground[1]
        endP = #d_ground - 1
        endX = d_ground[#d_ground - 1]
        for i = 1, #arg, 2 do
            if (arg[i] == "startP") then
                startP = arg[i + 1]
                startX = nil
            elseif (arg[i] == "startX") then
                if (startX ~= nil) then
                    startX = arg[i + 1]
                    startP = nil
                end
            end
            if (arg[i] == "endP") then
                endP = arg[i + 1]
                endX = nil
            elseif (arg[i] == "endX") then
                if (endX ~= nil) then
                    endX = arg[i + 1]
                    endP = nil
                end
            end
        end
    else
        d_ground = arg[1]
        startP = arg[2]
        if (arg[3]) then
            endP = arg[3]
        end
    end
    if (startP) then
        if (endP == nil) then
            local i = startP
            while (d_ground[i] <= endX) do
                i = i + 2
                endP = i
            end
        end
    else
        if (endP) then
            local top = math.floor(endP / 2)
            local bottum = 1
            while (bottum ~= top) do
                if (startX > d_ground[math.floor(top - bottum / 2 + bottum) * 2]) then
                    bottum = math.floor(top - bottum / 2 + bottum) + 1
                else
                    top = math.floor(top - bottum / 2 + bottum)
                end
            end
            startP = bottum
            top, bottum, startX = nil, nil, nil
            for i = startP, endP - startP + 1 do
                rtnGround[#rtnGround + 1] = d_ground[i]
            end
            if ((endP - startP + 1) % 2 ~= 0) then
                table.remove(rtnGround, #rtnGround)
            end
        else
            local top = #d_ground / 2
            local bottum = 1
            while (bottum ~= top) do
                if (startX > d_ground[math.floor(top - bottum / 2 + bottum) * 2]) then
                    bottum = math.floor(top - bottum / 2 + bottum) + 1
                else
                    top = math.floor(top - bottum / 2 + bottum)
                end
            end
            startP = bottum * 2
            top = #d_ground / 2
            while (bottum ~= top) do
                if (startX > d_ground[math.floor(top - bottum / 2 + bottum) * 2]) then
                    bottum = math.floor(top - bottum / 2 + bottum) + 1
                else
                    top = math.floor(top - bottum / 2 + bottum)
                end
            end
            endP = bottum * 2
            top, bottum, startX = nil, nil, nil
        end
    end
    for i = startP, endP - startP + 1 do
        rtnGround[#rtnGround + 1] = d_ground[i]
    end
    if ((endP - startP + 1) % 2 ~= 0) then
        table.remove(rtnGround, #rtnGround)
    end
end
local applyGround = function()
end
local modGround = function()
end
--angle = atan2(-y,x)
local makeGround = function(...)
end
rtn.getCountrySide = function()
    return jsonRoadsContents
end

return rtn
--[[
700,
300,
650,
300,
600,
300,
550,
300,
500,
300,
450,
300,
400,
300,
350,
300,
300,
300,
250,
300,
200,
300,
150,
295,
140,
290,
132,
285,
122,
281,
110,
279,
100,
283,
80,
290,
50,
300,
40,
302,
30,
302,
20,
304,
10,
308,
0,
310,
-10,
310,
-20,
308,
-30,
308,
-40,
311,
-50,
315,
-60,
322,
-70,
330,
-80,
340,
-90,
345,
-100,
345,
-150,
345,
-200,
345,
-250,
345,
-300,
345,
-350,
345,
-400,
345,
-450,
345,
-500,
345,
-550,
345,
-600,
310,
-650,
310,
-700,
310,
-750,
310,
-800,
275,
-850,
275,
-900,
275,
-950,
270,
-1000,
250,
-1050,
250,
-1100,
250,
-1150,
250,
-1200,
250,
-1250,
250,
-1300,
250,
-1350,
250,
-1400,
250
--]]
